// Lib
import { expect } from "chai";
import { describe, it } from "mocha";

// Mock
import { FSMock } from "../mock/fs.mock";

// Services
import { EnvService } from "./env.service";

describe("EnvService", () => {
  it("Should be able to read from .env file", () => {
    const fsMock = new FSMock("FOO=bar\nHELLO=world", true);
    const envService = new EnvService("", fsMock);
    expect(envService.get("FOO", "")).equal("bar");
    expect(envService.get("HELLO", "")).equal("world");
    expect(envService.get("FOOBAR", "foobar")).equal("foobar");
  });
  it("Should be able to get env vars from process", () => {
    const fsMock = new FSMock("", true);
    const envService = new EnvService("", fsMock);
    process.env.FOO = "bar";
    expect(envService.get("FOO", "")).equal("bar");
    delete process.env.FOO;
  });
  it("Should prioritise vars from process env variables over .env file", () => {
    const fsMock = new FSMock("FOO=bar\nHELLO=world", true);
    const envService = new EnvService("", fsMock);
    process.env.FOO = "bar2";
    expect(envService.get("FOO", "")).equal("bar2");
    delete process.env.FOO;
  });
  it("Should be able to get parse .env values having the = sign", () => {
    const fsMock = new FSMock("FOO==bar\nHELLO=wo=rld", true);
    const envService = new EnvService("", fsMock);
    expect(envService.get("FOO", "")).equal("=bar");
    expect(envService.get("HELLO", "")).equal("wo=rld");
    expect(envService.get("FOOBAR", "foobar")).equal("foobar");
  });
  it("Should be able to be used without fs path", () => {
    const envService = new EnvService();
    process.env.FOO = "bar";
    expect(envService.get("FOO", "")).equal("bar");
    delete process.env.FOO;
  });
  it("Should be able to work even if path doesn't exists", () => {
    const fsMock = new FSMock("FOO==bar\nHELLO=wo=rld", false);
    const envService = new EnvService("", fsMock);
    expect(envService.get("FOO", "default")).equal("default");
  })
});