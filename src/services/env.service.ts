// Interfaces
export interface IEnv { [key: string]: string; }
export interface IEnvService {
  get(name: string, defaultValue: string): string;
}


export type FsModuleWatchEventType = 'rename' | 'change';
export type FsModuleWatchListener<T> = (event: FsModuleWatchEventType, filename: T | null) => void;
export interface IFsModule {
  watch: (filename: string, listener: FsModuleWatchListener<string>) => IFSWatcher;
  readFileSync: (path: string, encoding: BufferEncoding) => string;
  existsSync: (path: string) => boolean;
}

export interface IFSWatcher {
  close: () => void;
}


/**
 * EnvService is used for retriving environment variables from a specified .env
 * file or any env variales specifed during execution
 */
export class EnvService implements IEnvService {
  private watcher?: IFSWatcher;

  private _data: IEnv = {};

  /**
   *
   * @param path Path to .env file
   * @param fsModule FS module to read and watch file system or false if no .env file is to be used
   * @param Should
   */
  public constructor();
  public constructor(path: string, fsModule: IFsModule);
  public constructor(
    private readonly path?: string,
    private readonly fsModule?: IFsModule,
  ) {
    // Check for env file if exists
    if (this.path !== undefined && this.fsModule !== undefined && this.fsModule.existsSync(this.path) === true) {
      // Sets data
      this._data = this.readData(this.path, this.fsModule);
      // initiate watcher which will updated envs on file change
      this.watcher = this.fsModule.watch(this.path, ((path: string, fsModule: IFsModule) => () => this._data = this.readData(path, fsModule))(this.path, this.fsModule));
    }
  }

  /**
   * Get env variable value with specific name
   * @param name The name of the env variable
   * @param defaultValue If variable is not found this default value will be returned instead
   * @return Return string value
   */
  public get(name: string, defaultValue: string): string {
    return process.env[name] || this._data[name] || defaultValue;
  }

  public close(): void {
    if (this.watcher !== undefined) {
      this.watcher.close();
    }
  }

  /**
   * Read and return formated env file
   * @param path Path to read from
   * @return formatted env variables
   */
  private readData(path: string, fsModule: IFsModule): IEnv {
    return fsModule.readFileSync(path, "utf8").split("\n").reduce((previousValue: {}, currentValue: string) => {
      const match = currentValue.match(/^([^=]+)=(.*)$/);
      return Object.assign(previousValue, match !== null ? { [match[1]]: match[2] } : {});
    }, {});
  }
}
