import { IFsModule, FsModuleWatchListener } from "../services/env.service";

export class FSMock implements IFsModule {
  public constructor(
    private readonly readFileSyncMockReturn: string,
    private readonly existsSyncReturn: boolean,
  ) { }

  public readFileSync(path: string, options: { encoding: BufferEncoding; flag?: string; } | BufferEncoding): string {
    return this.readFileSyncMockReturn;
  }

  public watch(filename: string, listener: FsModuleWatchListener<string>): any {
    return;
  }

  public existsSync(path: string): boolean { return this.existsSyncReturn; };

}
